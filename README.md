# FW Test

## Installation

1. First, you need clone this repository in your apache www folder.
2. In your project folder, execute `composer install`
3. Then, you should create a mysql database and execute next query:
```
#!mysql

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `books` (`id`, `name`) VALUES
(1, 'Book 1'),
(2, 'Book 2'),
(3, 'Book 3');

ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);
```
4. Edit `app/config/database.php` and edit with your database configuration.

Done! You can try FW Test.

## Demo

This is a simple PHP framework that uses many components from other frameworks like http-foundation, http-kernel and routing (Symfony) and Eloquent (Laravel).

In this project you can try 3 routing/controllers:

/   ->   DefaultController: Only show a simple message and 2 links to other controllers

/books   ->   BooksController: Shows a table with books from database

/musics   ->   MusicsController: Show a table with top 10 musics, loading info from last.fm API. Information is loaded by an ajax request when you click on "Load Data" link.