<?php

namespace Framework\Exception;

class FrameworkExceptionHandler implements \Illuminate\Contracts\Debug\ExceptionHandler
{
    public function report(\Exception $e)
    {
        //
    }

    public function render($request, \Exception $e)
    {
        throw $e;
    }

    public function renderForConsole($output, \Exception $e)
    {
        throw $e;
    }
}