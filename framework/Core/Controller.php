<?php

namespace Framework\core;

use Symfony\Component\HttpFoundation\Response;

class Controller
{
    protected function renderTemplate($templateName, $params = array()) {
        extract($params, EXTR_SKIP);
        ob_start();
        include sprintf(__DIR__.'/../../app/views/%s.html', $templateName);

        return new Response(ob_get_clean());
    }

    protected function renderJson($array) {
        $response = new Response();
        $response->setContent(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}