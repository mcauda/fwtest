<?php

namespace Framework\core;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel;
use Symfony\Component\Routing;
use Illuminate\Database\Capsule\Manager as Capsule;
use Framework\Exception;

class Core implements HttpKernel\HttpKernelInterface
{
    protected $routes = array();

    public function handle(Request $request, $type = HttpKernel\HttpKernelInterface::MASTER_REQUEST, $catch = true)
    {
        $routes = new Routing\RouteCollection();

        $rules = Config::getInstance()->read('routing');
        foreach ($rules as $ruleName => $rule) {
            $methods = array();
            if (isset($rule['methods'])) {
                $methods = $rule['methods'];
            }
            $routes->add($ruleName, new Routing\Route($rule['path'], $rule['defaults'], array(), array(), '', array(), $methods));
        }

        $context = new Routing\RequestContext();
        $context->fromRequest($request);
        $matcher = new Routing\Matcher\UrlMatcher($routes, $context);

        $controllerResolver = new HttpKernel\Controller\ControllerResolver();
        $argumentResolver = new HttpKernel\Controller\ArgumentResolver();

        /*  */
        $capsule = new Capsule();
        $capsule->getContainer()->singleton(
            'Illuminate\Contracts\Debug\ExceptionHandler',
            'Framework\Exception\FrameworkExceptionHandler'
        );
        $capsule->addConnection([
            'driver' => Config::getInstance()->read('database.driver'),
            'host' => Config::getInstance()->read('database.host'),
            'database' => Config::getInstance()->read('database.db_name'),
            'username' => Config::getInstance()->read('database.user'),
            'password' => Config::getInstance()->read('database.password'),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ]);

        $capsule->bootEloquent();

        try {
            $request->attributes->add($matcher->match($request->getPathInfo()));

            $controller = $controllerResolver->getController($request);

            $arguments = $argumentResolver->getArguments($request, $controller);

            $response = call_user_func_array($controller, $arguments);
        } catch (Routing\Exception\ResourceNotFoundException $e) {
            $response = new Response('Not Found', 404);
        } catch (Exception $e) {
            $response = new Response('An error occurred', 500);
        }

        return $response;
    }

}