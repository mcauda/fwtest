<?php

namespace Framework\core;

class Config {
    /* singleton instance */

    /**
     * @var Config
     */
    private static $instance = false;

    /* base config container. */
    private $config = array();


    // -------------------------------------------------------------------------
    /**
     *
     * @return Config
     */
    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new Config();
        }

        return self::$instance;
    }

    // -------------------------------------------------------------------------
    /**
     *
     * @param  string $key
     * @param  mixed  $value
     * @return boolean
     */
    public function set($key = false, $value = false) {
        $self = Config::getInstance();

        if (empty($key)) {
            throw new \Exception('Empty key');
        }

        $self->config[$key] = $value;

        return true;
    }

    // -------------------------------------------------------------------------
    /**
     *
     * @param  string $key
     * @return mixed
     */
    public function read($key = false) {
        $self = Config::getInstance();

        if (empty($key)) {
            throw new \Exception('Empty key');
        }

        if (!isset($self->config[$key])) {
            throw new \Exception("Undefined key [$key]");
        }

        return $self->config[$key];
    }

    // -------------------------------------------------------------------------
    public function exists($key) {
        try {
            $this->read($key);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    // -------------------------------------------------------------------------
    /**
     *
     * @param  array $configArray
     * @return boolean
     * @throws \Exception
     */
    public function load($configFile) {
        $self = Config::getInstance();

        $ret = include_once($configFile);
        if (empty($ret)) {
            throw new \Exception('Empty config file');
        }

        $self->config = array_merge($self->config, $config);

        return true;
    }

}
