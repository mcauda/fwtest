<?php

$config = array(
    'routing'  => array(
        'default' => array(
            'path' => '/',
            'defaults' => array(
                '_controller' => 'App\Controllers\DefaultController::indexAction'
            ),
        ),
        'books' => array(
            'path' => '/books',
            'defaults' => array(
                '_controller' => 'App\Controllers\BooksController::indexAction'
            ),
        ),
        'musics' => array(
            'path' => '/musics',
            'defaults' => array(
                '_controller' => 'App\Controllers\MusicsController::indexAction'
            ),
        ),
        'ajaxMusics' => array(
            'path' => '/ajaxMusicsAction',
            'defaults' => array(
                '_controller' => 'App\Controllers\MusicsController::ajaxMusicsAction'
            ),
            'methods' => array('POST')
        )
    )
);
