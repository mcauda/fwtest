<?php

namespace App\Models;

use Framework\core\Model;

class Book extends Model {

    protected $fillable = array('id', 'name');

}