<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Framework\core\Controller;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {

        return $this->renderTemplate('default');
    }
}