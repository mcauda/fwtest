<?php

namespace App\Controllers;

use App\Models\Book;
use Symfony\Component\HttpFoundation\Request;
use Framework\core\Controller;

class BooksController extends Controller
{
    public function indexAction(Request $request)
    {
        $books = Book::all()->toArray();
        
        return $this->renderTemplate('books', array('books' => $books));
    }
}