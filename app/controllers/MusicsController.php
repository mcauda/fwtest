<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Framework\core\Controller;
use LastFmApi\Api;

class MusicsController extends Controller
{
    public function indexAction(Request $request)
    {

        return $this->renderTemplate('musics');
    }

    public function ajaxMusicsAction(Request $request) {
        $auth = new Api\AuthApi('setsession', array('apiKey' => \Framework\core\Config::getInstance()->read('lastFm.apiKey')));
        $geoApi = new Api\GeoApi($auth);
        $musics = $geoApi->getTopArtists(array(
                'country' => 'Spain',
                'limit' => 10)
        );

        return $this->renderJson($musics);
    }
}