$(function() {
    function drawTable(data) {
        $("#musicsTable tr").remove();
        for (var i = 0; i < data.length; i++) {
            var row = $("<tr />")
            $("#musicsTable").append(row);
            row.append($('<td><img src="' + data[i].image.small + '"</td>'));
            row.append($('<td>' + data[i].name + '</td>'));
        }
    }

    function loadData() {
        $.ajax({
            url: 'ajaxMusicsAction',
            type: "post",
            dataType: "json",
            success: function(data) {
                drawTable(data);
            }
        });
    }

    $('#loadMusicsTable').click(function(e) {
        e.preventDefault();

        loadData();
    });

});
