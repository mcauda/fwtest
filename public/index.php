<?php

$loader = require __DIR__ . '/../vendor/autoload.php';
$loader->register();

if (file_exists(__DIR__ . '/../app/config/config.php')) {
    Framework\core\Config::getInstance()->load(__DIR__ . '/../app/config/config.php');
}
if (file_exists(__DIR__ . '/../app/config/routing.php')) {
    Framework\core\Config::getInstance()->load(__DIR__ . '/../app/config/routing.php');
}
if (file_exists(__DIR__ . '/../app/config/database.php')) {
    Framework\core\Config::getInstance()->load(__DIR__ . '/../app/config/database.php');
}

use Symfony\Component\HttpFoundation\Request;

$app = new Framework\core\Core();

$request = Request::createFromGlobals();

$response = $app->handle($request);
$response->send();
